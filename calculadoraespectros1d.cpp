#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <iomanip>
#include "savitzkygolay.h"

using namespace std;


struct tripla
{
	/*
	Estructura de datos que serve para almacenar os índices l, m e r dun triplete
	*/
	
	int l, m, r;
};

struct parametrosLorentz
{
	/*
	Estructura de datos que serve para almacenar os parámetros da curva lorentziana
	*/

	double l, A, w;
};

int countColumns(string row)
{
	/*
	Función que conta o número de columnas dunha cadea de caracteres
	*/

    int  numberOfColumns=1;
    bool previousWasSpace=false;

    for(int i=0; i<row.size(); i++)
    {
        if(row[i] == ' ' || row[i] == '\t' || row[i] == ';' || row[i] == ',')
        {
            if(!previousWasSpace)
                numberOfColumns++;

            previousWasSpace = true;
        }
        else
        {
            previousWasSpace = false;
        }   
    }   

    return numberOfColumns;
}

double gaussrand(double desviacion_estandar)
{
	/*
	Esta función xera un número aleatorio seguindo unha distribución normal
	*/

	static double V1, V2, S;
	static int phase = 0;
	double X;

	if(phase == 0) {
		do {
			double U1 = (double)rand() / RAND_MAX;
			double U2 = (double)rand() / RAND_MAX;

			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
			} while(S >= 1 || S == 0);

		X = V1 * sqrt(-2 * log(S) / S);
	} else
		X = V2 * sqrt(-2 * log(S) / S);

	phase = 1 - phase;

	return X * desviacion_estandar;
}

parametrosLorentz calculaparam(double *W, double y1, double y2, double y3, tripla p)
{
	/*
	Función para calcular os parámetros da lorentziana dentro do bucle correspondente
	*/

	double _w, _l, _A, w12, w13, w23, y12, y23, y13, w1, w2, w3;
	parametrosLorentz param;

	w1 = *(W + p.l);
	w2 = *(W + p.m);
	w3 = *(W + p.r);

	w12 = w1 - w2;
	w13 = w1 - w3;
	w23 = w2 - w3;

	y12 = y1 - y2;
	y23 = y2 - y3;
	y13 = y1 - y3;



	_w = (pow(w1, 2) * y1 * y23 + pow(w3, 2) * y3 * y12 + pow(w2, 2) * y2 * (-y13)) / 
		(2 * w12 * y1 * y2 - 2 * y3 * (y1 * w13 - y2 * w23));

	_l = pow(-y12 / (y1 * pow(w1 - _w, 2) - y2 * pow(w2 - _w,2)), -0.5);

	_A = - 4 * w12 * w13 * w23 * y1 * y2 * y3 * (w1 * y1 * y23 + w3 * y12 * y3 + w2 * y2 * (- y13)) * _l /
		(pow(w12, 4) * pow(y1 * y2, 2) - 2 * pow(w12, 2) * y1 * y2 * (pow(w13, 2) * y1 + pow(w23, 2) * y2) *
		y3 + pow(pow(w13, 2) * y1 - pow(w23, 2) * y2, 2) * pow(y3, 2));


	param.l = _l;
	param.A = _A;
	param.w = _w;

	return param;
}

class espectro
{
	private:
		int np;	//número de elementos do espectro
		double *S;	//espectro
		double *S_dd;	//segunda derivada do espectro
		double delt; //parámetro umbral
		double *W;	//dominio
		double lamb; //parámetro das loretnzianas necesario para establecer a rexión libre de sinal

		double Score(tripla p) //función que computa a puntuación
		{
			double suma_L = 0, suma_R = 0;

			for(int i = p.l; i <= p.r; i++)
			{
				if(i <= p.m)
				{
					suma_L += abs(*(S_dd + i));
				}
				if(i >= p.m)
				{
					suma_R += abs(*(S_dd + i));
				}
			}
			if(suma_L < suma_R)
			{
				return suma_L;
			}
			else
			{
				return suma_R;
			}
		}
	
	public:
		vector<tripla> L; //tripletes sen filtrar
		vector<tripla> LL; //tripletes filtrados
		vector<parametrosLorentz> J; //vector que garda os parámetros de cada lorentziana 
		vector<double> scores;

		espectro(double *spectrum, double *derivada, double *dominio, int n, double delta)
		{
			/*
			Constructor
			*/

			np = n;
			S = spectrum;
			S_dd = derivada;
			W = dominio;
			delt = delta;
		}

		~espectro()
		{
			/*
			Destructor
			*/
		}

		void Guarda(double *spectrum, double *derivada, double *dominio, int n, double delta)
		{
			/*
			Función que almacena o espectro e computa a segunda derivada.
			*/

			np = n;
			S = spectrum;
			S_dd = derivada;
			W = dominio;
			delt = delta;

		}

		void Lee()
		{
			/*
			Función que almacena nun ficheiro os valores do espectro así como as súa derivada segunda
			*/

			ofstream ficheiroSaida;
			ficheiroSaida.open ("espectro.txt");
			ficheiroSaida << "Os valores que toma o espectro son: " << endl;
			for(int i = 0; i < np; i++)
			{
				ficheiroSaida << *(S + i) << endl;
			}
			ficheiroSaida << "Os valores que toma a segunda derivada son: " << endl;
			for(int i = 0; i < np; i++)
			{
				ficheiroSaida << *(S_dd + i) << endl;
			}
			ficheiroSaida.close();
		}

		void PlotPuntuacion()
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar as puntuacións con gnuplot
			*/

			int k;
			ofstream ficheiro;
			ficheiro.open("plotPuntuacion.dat");
			for(int i = 0; i < L.size(); i++)
			{
				k = L[i].m;
				ficheiro << *(W + k) << "	" << scores[i] << endl;
			}
			ficheiro.close();
		}

		void PlotEspectro()
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar o espectro con gnuplot
			*/

			ofstream ficheiroPlotEspectro;
			ficheiroPlotEspectro.open("plotEspectro.dat");
			for(int i = 0; i < np; i++)
			{
				ficheiroPlotEspectro << *(W + i) << "	" << setprecision(15) << *(S + i) << endl;
			}
			ficheiroPlotEspectro.close();
		}

		void PlotDerivada()
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar a derivada do espectro
			*/
			ofstream ficheiroPlotDerivada;
			ficheiroPlotDerivada.open("plotDerivada.dat");
			for(int i = 0; i < np - 2; i++)
			{
				ficheiroPlotDerivada << *(W + i) << "	" << setprecision(15) << *(S_dd + i) << endl;
			}
			ficheiroPlotDerivada.close();
		}

		void PlotPosicionPicos()
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar a posición dos picos
			*/

			ofstream ficheiroPicos;
			ficheiroPicos.open("plotPicos.dat");
			for(int i = 0; i < np; i++)
			{
				for(int j = 0; j < LL.size(); j++)
				{
					if(i == LL[j].m)
					{
						ficheiroPicos << *(W + i) << "	" << setprecision(15) << 0.04 << endl;
					}
				}
			}
			ficheiroPicos.close();	
		}

		void PlotNovoEspectro()
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar o espectro reconstruido
			*/

			double *Q;
			Q = new double[np];


			for(int i = 0; i < np; i++)
			{
				for(int j = 0; j < J.size(); j++)
				{
					*(Q + i) += J[j].A * J[j].l / (pow(J[j].l, 2) + pow((*(W + i) - J[j].w), 2));
				}
			}

			for(int i = 0; i < J.size(); i++)
			{
				cout << J[i].A << "	" << J[i].l << "	" << J[i].w << endl;
			}

			ofstream ficheiroEspectro;
			ficheiroEspectro.open("plotNovoEspectro.dat");
			for(int i = 0; i < np; i++)
			{
				ficheiroEspectro << *(W + i) << "	" << setprecision(15) << *(Q + i) << endl;
			}
			ficheiroEspectro.close();	

			delete[] Q;
		}

		void ArquivoParametros()
		{
			ofstream ficheiroParametros;
			ficheiroParametros.open("Parametros.dat");
			for(int i = 0; i < J.size(); i++)
			{
				ficheiroParametros << J[i].w << " " << J[i].A << " " << " " << J[i].l << endl;
			}
			ficheiroParametros.close();
		}

		void Tripletes()
		{
			/*
			Función que calcula os tripletes do espectro
			*/

			int l, m, r;
			tripla p;
			double score, max_score = 0.0, score_media = 0.0;


			for(int i = 1; i < np - 3; i++)
			{
				if((*(S_dd + i) < 0 && *(S_dd + i - 1) > *(S_dd + i)) && *(S_dd + i + 1) > *(S_dd + i)) //buscamos mínimos locais negativos
				{
					m = i;

					for(int j = m - 1; j >= 1; j--)
					{
						if(((*(S_dd + j - 1) >= 0 && *(S_dd + j) < 0)) || ((*(S_dd + j - 1) <= *(S_dd + j) && *(S_dd + j + 1) < *(S_dd + j))))//raíz máis próxima ou máximo local máis próximo
						{
							l = j;
							break;
						}
					}

					for(int j = m + 1; j < np - 3; j++)
					{
						if(((*(S_dd + j + 1) >= 0 && *(S_dd + j) < 0)) || ((*(S_dd + j - 1) < *(S_dd + j) && *(S_dd + j) >= *(S_dd + j + 1))))
						{
							r = j;
							break;
						}
					}

					p.l = l;
					p.m = m;
					p.r = r; //almaceno en p l, m e r
					score = Score(p);

					L.push_back(p); //introduzo a tripla no vector
					scores.push_back(score);
				}
			}

			for(int i = 0; i < scores.size(); i++)
			{
				if(scores[i] > max_score)
				{
					max_score = scores[i];
				}
				score_media += scores[i];
			}

			for(int i = 0; i < scores.size(); i++)
			{
				scores[i] /= max_score;
			}
			score_media /= max_score * scores.size();

			//Determinación da zona sen sinal:
			double rms;
			vector<double> RMS;

			for(int i = 0; i < np; i++)
			{
				rms += pow(*(S + i), 2);
				if(i % (np / 100) == 0)
				{
					rms = sqrt(rms);
					RMS.push_back(rms);
					rms = 0.0;
				}
			}

			sort(RMS.begin(), RMS.end());

			// Normalizo
			for(int i = 0; i < RMS.size(); i++)
			{
				RMS[i] /= RMS.back();
			}

			
			double mediana = RMS[RMS.size() / 2];

			RMS.clear();
			
			for(int i = 0; i < scores.size(); i++)
			{
				if(scores[i] >= score_media + delt * 2 * mediana)
				{
					LL.push_back(L[i]);
				}
			}
		}

		void AproximacionProporcional()
		{
			/*
			Función que calcula os parámetros do espectro
			*/

			double Yl, Ym, Yr, yl, ym, yr, suml, summ, sumr;
			parametrosLorentz param;
			int maxIt = 1000, contit = 0;
			bool it;

			for(int i = 0; i < LL.size(); i++)
			{
				/*
				Neste bucle facemos unha primeira aproximación dos parámetros para comezar a iterar
				*/

				param = calculaparam(W, *(S + LL[i].l), *(S + LL[i].m), *(S + LL[i].r), LL[i]);

				J.push_back(param);
			}

			for(int i = 0; i < maxIt; i ++)
			{
				// Aquí iteramos ata que o resultado sexa o desexado

				for(int j = 0; j < LL.size(); j++)
				{
					it = true;

					Yl = *(S + LL[j].l);
					Ym = *(S + LL[j].m);
					Yr = *(S + LL[j].r);

					for(int k = 0; k < J.size(); k++)
					{
						yl = J[k].A * J[k].l / (pow(J[k].l,2) + pow((LL[j].l - J[k].w), 2));
						ym = J[k].A * J[k].l / (pow(J[k].l,2) + pow((LL[j].m - J[k].w), 2));
						yr = J[k].A * J[k].l / (pow(J[k].l,2) + pow((LL[j].r - J[k].w), 2));

						if(!(yl <= *(S + LL[j].l) && ym <= *(S + LL[j].m) && yr <= *(S + LL[j].r)))
						{
							it = false;
						}
					}

					if(it)
					{
						contit += 1;
						suml = 0;
						summ = 0;
						sumr = 0;
						for(int k = 0; k < J.size(); k++)
						{
							//	Calculo as sumas
							suml += J[k].A * J[k].l / (pow(J[k].l, 2) + pow((*(W + LL[j].l) - J[k].w), 2));
							summ += J[k].A * J[k].l / (pow(J[k].l, 2) + pow((*(W + LL[j].m) - J[k].w), 2));
							sumr += J[k].A * J[k].l / (pow(J[k].l, 2) + pow((*(W + LL[j].r) - J[k].w), 2));
						}

						//	Calculo as novas alturas
						Yl *= *(S + LL[j].l) / suml;
						Ym *= *(S + LL[j].m) / summ;
						Yr *= *(S + LL[j].r) / sumr;

						param = calculaparam(W, Yl, Ym, Yr, LL[j]);

						J[j] = param;
					}
				}
			}

			cout << "iteracions: " << contit / 100 << endl;
		}
};

int main()
{
	double *s, *dd, *w;
	string nome, linea, comando;
	int columnas, num_w;
	double delta = 3;
	bool arquivo = true, calculo = false;

	ifstream titulo("titulo.txt");
	string lina;
	while(getline(titulo, lina))
	{
		cout << lina << endl;
	}
	titulo.close();


	cout << "----------------------------------------------------------------" << endl;
	cout << "---------------------Pablo Sánchez González---------------------" << endl;
	cout << "----------------------------------------------------------------" << endl << endl << endl;

	cout << "Introduza o nome do arquivo que contén o espectro (incluíndo a extensión): " << endl;

	cin >> nome;

	ifstream file(nome);

	if(!file.is_open())
	{
		arquivo = false;
	}

	bool incorrecto = false;
	while(!arquivo)
	{
		nome.erase();
		cout << "Nome incorrecto, introduza o nome do arquivo:" << endl;
		cin >> nome;
		ifstream file(nome);
		arquivo = file.is_open();
		incorrecto = true;
	}
	if(incorrecto)
	{
		file.open(nome);
	}
	getline(file, linea);

	columnas = countColumns(linea);

	double x, y;
	vector<double> X, Y, YY;

	if(columnas == 1)
	{
		int i = 1;
		while(file >> y)
		{
			X.push_back(i);
			Y.push_back(y);
			i++;
		}
	}
	if(columnas == 2)
	{
		while(file >> x >> y)
		{
			X.push_back(x);
			Y.push_back(y);
		}
	}
	else
	{
		cout << "O formato do arquivo é incorrecto." << endl;
	}
	file.close();

	num_w = X.size();
	s = &Y[0];
	w = &X[0];
	dd = new double[num_w];

	espectro spec(s, dd, w, num_w, delta);

	/*
	A continuación declaranse os comandos dos que dispón o programa.
	*/

	string exit("exit"), ruido("ruido"), calcular("calcular"), parametros("parametros"), plotEspectro("plotEspectro"), 
	plotPuntuacion("plotPuntuacion"), plotDerivada("plotDerivada"), plotNovoEspectro("plotNovoEspectro"), axuda("axuda");
	
	bool bucle = true;

	cout << "Escriba axuda se desexa ver unha lista cos comandos dispoñibles." << endl;

	while(bucle)
	{
		cin >> comando;

		if(comando.compare(exit) == 0)
		{
			//Comando empregado para sair do bucle.
			bucle = false;		}

		else if(comando.compare(ruido) == 0)
		{
			//Comando empregado para engadir ruído.
			cout << "Introduza a desviación estándar do ruído:" << endl;
			double sigma;
			cin >> sigma;

			for(int i = 0; i < Y.size(); i++)
			{
				YY.push_back(gaussrand(sigma) + Y[i]);
			}

			s = &YY[0];
			cout << "Ruído engadido." << endl;
		}

		else if(comando.compare(calcular) == 0)
		{
			sgconvolution(s, dd, 5, num_w, SG5);
			spec.Guarda(s, dd, w, num_w, delta);
			spec.Tripletes();
			spec.PlotPuntuacion();
			spec.PlotEspectro();
			spec.AproximacionProporcional();
			cout << "Cálculo completado." << endl;
			calculo = true;
		}

		else if(comando.compare(parametros) == 0 && calculo)
		{
			spec.ArquivoParametros();
			cout << "Xerado o arquivo <<Parametros.dat>>" << endl;
		}

		else if(comando.compare(plotPuntuacion) == 0 && calculo)
		{
			spec.PlotPuntuacion();
			cout << "Xerado o arquivo <<plotPuntuacion.dat>>" << endl;
		}

		else if(comando.compare(plotDerivada) == 0 && calculo)
		{
			spec.PlotDerivada();
			cout << "Xerado o arquivo <<plotDerivada.dat>>" << endl;
		}

		else if(comando.compare(plotEspectro) == 0 && calculo)
		{
			spec.PlotEspectro();
			cout << "Xerado o arquivo <<plotEspectro.dat>>" << endl;
		}

		else if(comando.compare(plotNovoEspectro) == 0 && calculo)
		{
			spec.PlotNovoEspectro();
			cout << "Xerado o arquivo <<plotNovoEspectro.dat>>" << endl;
		}

		else if(comando.compare(axuda) == 0)
		{
			cout << "Os comandos dispoñibles son os que seguen:" << endl << endl;
			cout << "exit" << endl;
			cout << "Remata a execución do programa." << endl << endl;
			cout << "ruido" << endl;
			cout << "Engade ruido gausiano ao espectro." << endl << endl;
			cout << "calcular" << endl;
			cout << "Busca e parametriza os picos do espectro" << endl << endl;
			cout << "(Os seguintes comandos só se poden empregar despois de calcular)" << endl << endl;
			cout << "parametros" << endl;
			cout << "Xera un arquivo chamado <<Parametros.dat>> no que se almacenan por filas a posición a altura e a anchura dos picos do espectro." << endl << endl;
			cout << "plotPuntuacion" << endl;
			cout << "Xera un arquivo chamado <<plotPuntuacion.dat>> no que se almacenan os valores da puntuación de cada punto espectral" << endl << endl;
			cout << "plotDerivada" << endl;
			cout << "Xera un arquivo chamado <<plotDerivada.dat>> no que se almacena en formato X Y o cálculo da segunda derivada do espectro" << endl << endl;
			cout << "plotEspectro" << endl;
			cout << "Xera un arquivo chamado << plotEspectro.dat>> no que se almacena en formato X Y o espectro introducido" << endl << endl;
			cout << "plotNovoEspectro" << endl;
			cout << "Xera un arquivo chamado <<plotNovoEspectro.dat>> no que se almacena en formato X Y o espectro reconstruído" << endl << endl;
		}

		else
		{
			cout << "Ese comando non existe ou aínda non pode executarse." << endl;
		}
	}

	//Liberamos memoria.
	delete[] dd;
	X.clear();
	Y.clear();
	YY.clear();
	exit.clear();
	ruido.clear();
	calcular.clear();
	parametros.clear();
	plotEspectro.clear();
	plotPuntuacion.clear();
	plotDerivada.clear();
	plotNovoEspectro.clear();
	axuda.clear();

	return 0;
}