/*
Convolución para aplicar o método de Savitzky-Golay
*/

void sgconvolution(double *signal, double *result, int vent, int n, float *SG)
{
	for(int i = (vent - 1) / 2; i <= n - (vent - 1) / 2; i++)
	{
		for(int j = - (vent - 1) / 2; j <= (vent - 1) / 2; j++)
		{
			*(result + i) += *(signal + i + j) * *(SG + j + (vent - 1) / 2);
		}
	}
}

void sgconvolution2D(double **signal, double **result, int vent, int n, int m, float *SG)
{
	int contador;
	for(int i = (vent - 1) / 2; i < n - (vent - 1) / 2; i++)
	{
		for(int j = (vent - 1) / 2; j < m - (vent - 1) / 2; j++)
		{
			contador = 0;
			for(int h = -(vent - 1) / 2; h <= (vent - 1) / 2; h++)
			{
				for(int k = -(vent - 1) / 2; k <= (vent - 1) / 2; k++)
				{
					contador += 1;
					*(*(result + i) + j) += *(*(signal + i + h) + j + k) * *(SG + contador);
				}
			}
		}
	}
}


//Coeficientes para Sav-Gol en 1D 2ª derivada

static float SG3[] = { 1.f, -2.f, +1.f };

static float SG5[] = { 0.285714285714,
-0.142857142857,
-0.285714285714,
-0.142857142857,
0.285714285714 };

static float SG7[] = { 0.119047619047,
0,
-0.0714285714285,
-0.0952380952380,
-0.0714285714285,
0,
0.119047619047 };

static float SG9[] = { 0.0606060606060,
0.0151515151515,
-0.0173160173160,
-0.0367965367965,
-0.0432900432900,
-0.0367965367965,
-0.0173160173160,
0.0151515151515,
0.0606060606060 };

static float SG11[] = { 0.0349650349650,
0.0139860139860,
- 0.00233100233100,
- 0.0139860139860,
- 0.0209790209790,
- 0.0233100233100,
- 0.0209790209790,
- 0.0139860139860,
- 0.00233100233100,
0.0139860139860,
0.0349650349650
};

static float SG13[] = { 0.0219780219780,
0.0109890109890,
0.00199800199800,
- 0.00499500499500,
- 0.00999000999000,
- 0.0129870129870,
- 0.0139860139860,
- 0.0129870129870,
- 0.00999000999000,
- 0.00499500499500,
0.00199800199800,
0.0109890109890,
0.0219780219780
};