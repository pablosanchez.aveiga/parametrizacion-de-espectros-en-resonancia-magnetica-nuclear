#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <iomanip>
#include "savitzkygolay.h"
#include "SavGol.cpp"

using namespace std;


struct quintupla
{
	/*
	Estructura de datos que serve para almacenar os índices l, m e r dun triplete
	*/
	
	int l, m, n, r, u, d;
};

struct parametrosLorentz
{
	/*
	Estructura de datos que serve para almacenar os parámetros da curva lorentziana
	*/

	double lx, ly, A, wx, wy;
};

double gaussrand(double desviacion_estandar)
{
	/*
	Esta función xera un número aleatorio seguindo unha distribución normal
	*/

	static double V1, V2, S;
	static int phase = 0;
	double X;

	if(phase == 0) {
		do {
			double U1 = (double)rand() / RAND_MAX;
			double U2 = (double)rand() / RAND_MAX;

			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
			} while(S >= 1 || S == 0);

		X = V1 * sqrt(-2 * log(S) / S);
	} else
		X = V2 * sqrt(-2 * log(S) / S);

	phase = 1 - phase;

	return X * desviacion_estandar;
}

int countColumns(string row)
{
	/*
	Función que conta o número de columnas dunha cadea de caracteres
	*/

    int  numberOfColumns=1;
    bool previousWasSpace=false;

    for(int i=0; i<row.size(); i++)
    {
        if(row[i] == ' ' || row[i] == '\t' || row[i] == ';' || row[i] == ',')
        {
            if(!previousWasSpace)
                numberOfColumns++;

            previousWasSpace = true;
        }
        else
        {
            previousWasSpace = false;
        }   
    }   

    return numberOfColumns;
}

parametrosLorentz calculaparam(double *Wx, double *Wy, double y1, double y2, double y3, double y1_, double y3_, quintupla p)
{
	/*
	Función para calcular os parámetros da lorentziana dentro do bucle correspondente
	*/

	double _wx, _wy, _l, _A, _lx, _ly, _Ax, _Ay, w12, w13, w23, y12, y23, y13, w1, w2, w3, w1_, w2_, w3_, w12_, w13_, w23_, y12_, y23_, y13_, y2_ = y2;
	parametrosLorentz param;

	w1 = *(Wx + p.l);
	w2 = *(Wx + p.m);
	w3 = *(Wx + p.r);

	w1_ = *(Wy + p.d);
	w2_ = *(Wy + p.n);
	w3_ = *(Wy + p.u);

	w12 = w1 - w2;
	w13 = w1 - w3;
	w23 = w2 - w3;

	w12_ = w1_ - w2_;
	w13_ = w1_ - w3_;
	w23_ = w2_ - w3_;

	y12 = y1 - y2;
	y23 = y2 - y3;
	y13 = y1 - y3;

	y12_ = y1_ - y2_;
	y23_ = y2_ - y3_;
	y13_ = y1_ - y3_;



	_wx = (pow(w1, 2) * y1 * y23 + pow(w3, 2) * y3 * y12 + pow(w2, 2) * y2 * (-y13)) / 
		(2 * w12 * y1 * y2 - 2 * y3 * (y1 * w13 - y2 * w23));

	_wy = (pow(w1_, 2) * y1_ * y23_ + pow(w3_, 2) * y3_ * y12_ + pow(w2_, 2) * y2_ * (-y13_)) / 
		(2 * w12_ * y1_ * y2_ - 2 * y3_ * (y1_ * w13_ - y2_ * w23_));

	_lx = pow(-y12 / (y1 * pow(w1 - _wx, 2) - y2 * pow(w2 - _wx,2)), -0.5);

	_ly = pow(-y12_ / (y1_ * pow(w1_ - _wy, 2) - y2_ * pow(w2_ - _wy,2)), -0.5);

	_Ax = - 4 * w12 * w13 * w23 * y1 * y2 * y3 * (w1 * y1 * y23 + w3 * y12 * y3 + w2 * y2 * (- y13)) /
		(pow(w12, 4) * pow(y1 * y2, 2) - 2 * pow(w12, 2) * y1 * y2 * (pow(w13, 2) * y1 + pow(w23, 2) * y2) *
		y3 + pow(pow(w13, 2) * y1 - pow(w23, 2) * y2, 2) * pow(y3, 2));

	_Ay = - 4 * w12_ * w13_ * w23_ * y1_ * y2_ * y3_ * (w1_ * y1_ * y23_ + w3_ * y12_ * y3_ + w2_ * y2_ * (- y13_)) /
		(pow(w12_, 4) * pow(y1_ * y2_, 2) - 2 * pow(w12_, 2) * y1_ * y2_ * (pow(w13_, 2) * y1_ + pow(w23_, 2) * y2_) *
		y3_ + pow(pow(w13_, 2) * y1_ - pow(w23_, 2) * y2_, 2) * pow(y3_, 2));


	if(isnan(_lx))
	{
		_lx = pow(-y23 / (y3 * pow(w3 - _wx, 2) - y2 * pow(w2 - _wx,2)), -0.5);
	}

	if(isnan(_ly))
	{
		_ly = pow(-y23_ / (y3_ * pow(w3_ - _wy, 2) - y2_ * pow(w2_ - _wy,2)), -0.5);
	}

	_A = (y2_ * (1 + pow((w2_ - _wy) / _ly, 2)) + y2 * (1 + pow((w2 - _wx) / _lx, 2))) / 2;// Notese que este parametro e a altura da lorentziana, non a sua amplitude, xa que ten duas amplitudes.

	param.lx = _lx;
	param.ly = _ly;
	param.A = _A;
	param.wx = _wx;
	param.wy = _wy;

	return param;
}

class espectro
{
	private:
		int yp, xp;	//filas e columnas do dominio
		double **S;	//espectro
		double **S_xx;	//parcial segunda con respecto a x
		double **S_yy;	//parcial segunda con respecto a y
		double delt; //parámetro umbral
		double *Wx, *Wy;	//dominio

		double Score(quintupla p) //función que computa a puntuación
		{
			double suma_L = 0, suma_R = 0, suma_U = 0, suma_D = 0;
			for(int i = p.l; i <= p.r; i++)
			{
				if(i <= p.m)
				{
					suma_L += abs(*(*(S_xx + i) + p.n));
				}
				if(i >= p.m)
				{
					suma_R += abs(*(*(S_xx + i) + p.n));
				}
			}
			for(int j = p.d; j <= p.u; j++)
			{
				if(j <= p.n)
				{
					suma_D += abs(*(*(S_yy + p.m) + j));
				}
				if(j >= p.n)
				{
					suma_U += abs(*(*(S_yy + p.m) + j));
				}
			}

			return min(suma_U, min(suma_D, min(suma_R, suma_L)));
		}
	
	public:
		vector<quintupla> L; //tripletes sen filtrar
		vector<quintupla> LL; //tripletes filtrados
		vector<parametrosLorentz> J; //vector que garda os parámetros de cada lorentziana 
		vector<double> scores;

		espectro(double **spectrum, double **derivadaxx, double **derivadayy, double *dominiox, double *dominioy, int m, int n, double delta)
		{
			/*
			Constructor
			*/

			xp = m;
			yp = n;
			S = spectrum;
			S_xx = derivadaxx;
			S_yy = derivadayy;
			Wx = dominiox;
			Wy = dominioy;
			delt = delta;
		}

		~espectro()
		{
			/*
			Destructor
			*/
		}

		void Guarda(double **spectrum, double **derivadaxx, double **derivadayy, double *dominiox, double *dominioy, int m, int n, double delta)
		{
			/*
			Función que almacena o espectro e coxputa a segunda derivada.
			*/

			xp = m;
			yp = n;
			S = spectrum;
			S_xx = derivadaxx;
			S_yy = derivadayy;
			Wx = dominiox;
			Wy = dominioy;
			delt = delta;

		}

		void PlotPuntuacion(double umbral = 0)
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar as puntuacións con gnuplot
			*/

			ofstream ficheiro;
			ficheiro.open("plotPuntuacion.dat");
			for(int i = 0; i < L.size(); i++)
			{
				if(scores[i] > umbral) ficheiro << *(Wx + L[i].m) << "	" << *(Wy + L[i].n) << " " << scores[i] << endl;
			}
			ficheiro.close();
		}

		void PlotEspectro()
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar o espectro con gnuplot
			*/

			ofstream ficheiroPlotEspectro;
			ficheiroPlotEspectro.open("plotEspectro.dat");
			for(int i = 0; i < xp; i++)
			{
				for(int j = 0; j < yp; j++)
				{
					ficheiroPlotEspectro << *(Wx + i) << " " << *(Wy + j) << " " << setprecision(15) << *(*(S + i) + j) << endl;
				}
				ficheiroPlotEspectro << "\n";
			}
			ficheiroPlotEspectro.close();
		}

		void PlotDerivada()
		{
			/*
			Función que almacena en sendos ficheiros os datos necesarios para representar as derivadas parciais segundas
			*/
			ofstream ficheiroPlotDerivadax;
			ficheiroPlotDerivadax.open("plotDerivadax.dat");
			ofstream ficheiroPlotDerivaday;
			ficheiroPlotDerivaday.open("plotDerivaday.dat");
			for(int i = 0; i < xp; i++)
			{
				for(int j = 0; j < yp; j++)
				{
					ficheiroPlotDerivadax << *(Wx + i) << " " << *(Wy + j) << " " << setprecision(15) << *(*(S_xx + i) + j) << endl;
					ficheiroPlotDerivaday << *(Wx + i) << " " << *(Wy + j) << " " << setprecision(15) << *(*(S_yy + i) + j) << endl;
				}
				ficheiroPlotDerivadax << "\n";
				ficheiroPlotDerivaday << "\n";
			}
			ficheiroPlotDerivadax.close();
			ficheiroPlotDerivaday.close();
		}

		void PlotPosicionPicos()
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar a posición dos picos
			*/

			ofstream ficheiroPicos;
			ficheiroPicos.open("plotPicos.dat");
			for(int i = 0; i < LL.size(); i++)
			{
				ficheiroPicos << *(Wx + LL[i].m) << " " << *(Wy + LL[i].n) << endl;
			}
			ficheiroPicos.close();	
		}

		void PlotNovoEspectro()
		{
			/*
			Función que almacena nun ficheiro os valores necesarios para representar o espectro reconstruido
			*/

			double **Q;
			Q = new double*[xp];
			for(int i = 0; i < xp; i++)
			{
				*(Q + i) = new double[yp];
			}

			for(int i = 0; i < xp; i++)
			{
				for(int j = 0; j < yp; j++)
				{
					for(int k = 0; k < J.size(); k++)
					{
						*(*(Q + i) + j) += J[k].A / (1 + pow((*(Wx + i) - J[k].wx) / J[k].lx, 2) + pow((*(Wy + j) - J[k].wy) / J[k].ly, 2));
					}
				}
			}

			for(int i = 0; i < J.size(); i++)
			{
				cout << J[i].A << "	" << J[i].lx << " " << J[i].ly << " " << "	" << J[i].wx << " " << J[i].wy << endl;
			}

			ofstream ficheiroEspectro;
			ficheiroEspectro.open("plotNovoEspectro.dat");

			for(int j = 0; j < yp; j++)
			{
				for(int i = 0; i < xp; i++)
				{
					ficheiroEspectro << *(Wx + i) << " " << *(Wy + j) << " " << setprecision(15) << *(*(Q + i) + j) << endl;
				}
				ficheiroEspectro << "\n";
			}
			ficheiroEspectro.close();

			ofstream ficheiroDiferencias;
			ficheiroDiferencias.open("plotDiferencias.dat");

			for(int j = 0; j < yp; j++)
			{
				for(int i = 0; i < xp; i++)
				{
					ficheiroDiferencias << *(Wx + i) << " " << *(Wy + j) << " " << setprecision(15) << abs(*(*(Q + i) + j) - *(*(S + i) + j)) << endl;
				}
				ficheiroDiferencias << "\n";
			}
			ficheiroDiferencias.close();

			delete[] Q;
		}

		void ArquivoParametros()
		{
			ofstream ficheiroParametros;
			ficheiroParametros.open("Parametros.dat");

			for(int i = 0; i < J.size(); i++)
			{
				ficheiroParametros << J[i].wx << " " << J[i].wy << " " << J[i].A << " " << J[i].lx << " " << J[i].ly << endl;
			}
			ficheiroParametros.close();
		}

		void Tripletes()
		{
			/*
			Función que calcula os tripletes do espectro
			*/

			int l, m, n, r, u, d;
			quintupla p;
			double score, max_score = 0.0, score_media = 0.0;

			for(int i = 1; i < xp - 1; i++)
			{
				for(int j = 1; j < yp - 1; j++)
				{
					if(*(*(S_xx + i) + j) < 0 && *(*(S_xx + i - 1) + j) > *(*(S_xx + i) + j) && *(*(S_xx + i + 1) + j) > *(*(S_xx + i) + j)
						&& *(*(S_yy + i) + j) < 0 && *(*(S_yy + i) + j -1) > *(*(S_yy + i) + j) && *(*(S_yy + i) + j) < *(*(S_yy + i) + j + 1))
					{
						m = i;
						n = j;
						for(int ii = m - 1; ii >= 1; ii--)
						{
							if((*(*(S_xx + ii - 1) + n) >= 0 && *(*(S_xx + ii) + n) < 0) || (*(*(S_xx + ii - 1) + n) <= *(*(S_xx + ii) + n) && 
								*(*(S_xx + ii) + n) > *(*(S_xx + ii + 1) + n)))
							{
								l = ii;
								break;
							}
						}
						for(int ii = m + 1; ii < xp - 1; ii++)
						{
							if((*(*(S_xx + ii + 1) + n) >= 0 && *(*(S_xx + ii) + n) < 0) || (*(*(S_xx + ii - 1) + n) < *(*(S_xx + ii) + n) &&
								*(*(S_xx + ii) + n) >= *(*(S_xx + ii + 1) + n)))
							{
								r = ii;
								break;
							}
						}
						for(int jj = n - 1; jj >= 1; jj--)
						{
							if((*(*(S_yy + m) + jj - 1) >= 0 && *(*(S_yy + m) + jj) < 0) || (*(*(S_yy + m) + jj - 1) <= *(*(S_yy + m) + jj) && 
								*(*(S_yy + m) + jj) > *(*(S_yy + m) + jj + 1)))
							{
								d = jj;
								break;
							}
						}
						for(int jj = n + 1; jj < yp - 1; jj++)
						{
							if((*(*(S_yy + m) + jj + 1) >= 0 && *(*(S_yy + m) + jj) < 0) || (*(*(S_yy + m) + jj - 1) < *(*(S_yy + m) + jj) &&
								*(*(S_yy + m) + jj) >= *(*(S_yy + m) + jj + 1)))
							{
								u = jj;
								break;
							}
						}

						//	Almacenamos a quintupla
						p.l = l;
						p.m = m;
						p.n = n;
						p.r = r;
						p.u = u;
						p.d = d;
						//	Puntuamola
						score = Score(p);
						//	Gardamos a quintupla e a puntuación
						L.push_back(p);
						scores.push_back(score);
					}
				}
			}

			//	Normalizamos as puntuacións
			for(int i = 0; i < scores.size(); i++)
			{
				if(scores[i] > max_score)
				{
					max_score = scores[i];
				}
				score_media += scores[i];
			}

			for(int i = 0; i < scores.size(); i++)
			{
				scores[i] /= max_score;
			}
			score_media /= max_score * scores.size();

			//	Determinación da zona sen sinal
			double rms = 0.0;
			vector<double> RMS;
			int cont = 0;

			for(int i = 0; i < 10; i ++)
			{
				for(int j = 0; j < 10; j++) // Dividimos o dominio en dez trozos que imos recorrer individualmente
				{
					for(int ii = 0; ii < xp / 10; ii++)
					{
						for(int jj = 0; jj < yp / 10; jj++)
						{
							cont += 1;
							rms += pow(*(*(S + ii + i * xp / 10) + jj + j * yp / 10), 2);
						}
					}
					rms = sqrt(rms / cont);
					RMS.push_back(rms);
					rms = 0.0;
					cont = 0;
				}
			}

			// Calculamos a mediana
			sort(RMS.begin(), RMS.end());

			// Normalizo
			for(int i = 0; i < RMS.size(); i++)
			{
				RMS[i] /= RMS.back();
			}

			double mediana = RMS[RMS.size() / 2];
			RMS.clear();

			cout << score_media + delt * mediana << endl;

			bool resoluble;
			
			for(int i = 0; i < scores.size(); i++)
			{
				resoluble = false;

				if(*(*(S + L[i].m) + L[i].n) > 0 && *(*(S + L[i].l) + L[i].n) > 0 && *(*(S + L[i].r) + L[i].n) > 0 && *(*(S + L[i].m) + L[i].u) > 0 && *(*(S + L[i].m) + L[i].d) > 0)
				{
					resoluble = true;
				}

				if(scores[i] >= score_media + delt * mediana && resoluble)
				{
					LL.push_back(L[i]);
				}
			}
		}

		void AproximacionProporcional()
		{
			/*
			Función que calcula os parámetros do espectro
			*/

			double Yl, Ym, Yr, Yu, Yd, yl, ym, yr, yu, yd, suml, summ, sumr, sumd, sumu;
			parametrosLorentz param;
			int maxIt = 1000, contit = 0;
			bool it;

			for(int i = 0; i < LL.size(); i++)
			{
				/*
				Neste bucle facemos unha primeira aproximación dos parámetros para comezar a iterar
				*/

				param = calculaparam(Wx, Wy, *(*(S + LL[i].l) + LL[i].n), *(*(S + LL[i].m) + LL[i].n), *(*(S + LL[i].r) + LL[i].n),
					*(*(S + LL[i].m) + LL[i].d), *(*(S + LL[i].m) + LL[i].u), LL[i]);

				J.push_back(param);
			}

			for(int i = 0; i < maxIt; i ++)
			{
				// Aquí iteramos ata que o resultado sexa o desexado

				for(int j = 0; j < LL.size(); j++)
				{
					it = true;

					Yl = *(*(S + LL[j].l) + LL[j].n);
					Ym = *(*(S + LL[j].m) + LL[j].n);
					Yr = *(*(S + LL[j].r) + LL[j].n);
					Yd = *(*(S + LL[j].m) + LL[j].d);
					Yu = *(*(S + LL[j].m) + LL[j].u);

					for(int k = 0; k < J.size(); k++)
					{
						yl = J[k].A / (1 + pow((*(Wx + LL[j].l) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].n) - J[k].wy) / J[k].ly, 2));
						ym = J[k].A / (1 + pow((*(Wx + LL[j].m) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].n) - J[k].wy) / J[k].ly, 2));
						yr = J[k].A / (1 + pow((*(Wx + LL[j].r) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].n) - J[k].wy) / J[k].ly, 2));
						yd = J[k].A / (1 + pow((*(Wx + LL[j].m) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].d) - J[k].wy) / J[k].ly, 2));
						yu = J[k].A / (1 + pow((*(Wx + LL[j].m) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].u) - J[k].wy) / J[k].ly, 2));

						if(!(yl <= *(*(S + LL[j].l) + LL[j].n) && ym <= *(*(S + LL[j].m) + LL[j].n) && yr <= *(*(S + LL[j].r) + LL[j].n) &&
							yd <= *(*(S + LL[j].m) + LL[j].d) && yu <= *(*(S + LL[j].m) + LL[j].u)))
						{
							it = false;
							break;
						}
					}

					if(it)
					{
						contit += 1;
						suml = 0;
						summ = 0;
						sumr = 0;
						sumu = 0;
						sumd = 0;
						for(int k = 0; k < J.size(); k++)
						{
							//	Calculo as sumas
							suml += J[k].A / (1 + pow((*(Wx + LL[j].l) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].n) - J[k].wy) / J[k].ly, 2));
							summ += J[k].A / (1 + pow((*(Wx + LL[j].m) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].n) - J[k].wy) / J[k].ly, 2));
							sumr += J[k].A / (1 + pow((*(Wx + LL[j].r) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].n) - J[k].wy) / J[k].ly, 2));
							sumd += J[k].A / (1 + pow((*(Wx + LL[j].m) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].d) - J[k].wy) / J[k].ly, 2));
							sumu += J[k].A / (1 + pow((*(Wx + LL[j].m) - J[k].wx) / J[k].lx, 2) + pow((*(Wx + LL[j].u) - J[k].wy) / J[k].ly, 2));
						}

						//	Calculo as novas alturas
						Yl *= *(*(S + LL[j].l) + LL[j].n) / suml;
						Ym *= *(*(S + LL[j].m) + LL[j].n) / summ;
						Yr *= *(*(S + LL[j].r) + LL[j].n) / sumr;
						Yd *= *(*(S + LL[j].m) + LL[j].d) / sumd;
						Yu *= *(*(S + LL[j].m) + LL[j].u) / sumu;

						param = calculaparam(Wx, Wy, Yl, Ym, Yr, Yd, Yu, LL[j]);

						J[j] = param;
					}
				}
			}

			cout << "iteracions: " << contit / 100 << endl;
		}
};

int main()
{
	double **S, **S_xx, **S_yy;
	double *wx, *wy;
	double delta = 1;
	int columnas;
	string linea;

	ifstream file("espectro.txt");

	//	Reservamos memoria para almacenar o espectro
	getline(file, linea);
	getline(file, linea);
	columnas = countColumns(linea);

	file.clear();
	file.seekg(0);

	int filas = 0;
	while(file.good()) if(file.get() == '\n') filas++;


	filas -= 1;
	columnas -= 1;

	S = new double*[columnas];
	S_xx = new double*[columnas];
	S_yy = new double*[columnas];
	for(int i = 0; i < columnas; i++)
	{
		*(S + i) = new double[filas];
		*(S_xx + i) = new double[filas];
		*(S_yy + i) = new double[filas];
	}
 	
	file.clear();
	file.seekg(0);

	cout << filas << " " << columnas << endl;


	for(int j = 0; j < filas; j++)
	{
		for(int i = 0; i < columnas; i++)
		{
			file >> *(*(S + i) + j);
			//*(*(S + i) + j) = -*(*(S + i) + j);
 			// Engado ruído

			//*(*(S + i) + j) += gaussrand(0.5);
		}
	}

	file.close();
 	
	//	Calculamos as derivadas

	sgconvolution2D(S, S_xx, 5, columnas, filas, fSavGolSize5Order2X0Y2);
	sgconvolution2D(S, S_yy, 5, columnas, filas, fSavGolSize5Order2X2Y0);

	//	Creamos os vectores do dominio

	wx = new double[columnas];
	wy = new double[filas];

	for(int i = 0; i < columnas; i++)
	{
		*(wx + i) = i;
	}
	 	for(int i = 0; i < filas; i++)
	{
		*(wy + i) = i;
	}

	//	Creamos unha instancia de espectro

	espectro spec(S, S_xx, S_yy, wx, wy, columnas, filas, delta);
	spec.Tripletes();
	spec.PlotEspectro();
	spec.PlotDerivada();
	spec.PlotPuntuacion();
	spec.AproximacionProporcional();
	spec.ArquivoParametros();
	spec.PlotNovoEspectro();

	for(int i = 0; i < columnas; i++)
	{
		delete[] *(S + i);
		delete[] *(S_xx + i);
		delete[] *(S_yy + i);
	}


	delete[] S;
	delete[] S_xx;
	delete[] S_yy;
	delete[] wx;
	delete[] wy;

	return 0;
}