/*
Función que crea espectros 
*/
#include <iostream>
#include <stdlib.h> //números aleatorios
#include <time.h>
#include <math.h>
#include <fstream>
#include <iomanip>
#include <fstream>

using namespace std;


double gaussrand(double desviacion_estandar)
{
	/*
	Esta función xera un número aleatorio seguindo unha distribución normal
	*/

	static double V1, V2, S;
	static int phase = 0;
	double X;

	if(phase == 0) {
		do {
			double U1 = (double)rand() / RAND_MAX;
			double U2 = (double)rand() / RAND_MAX;

			V1 = 2 * U1 - 1;
			V2 = 2 * U2 - 1;
			S = V1 * V1 + V2 * V2;
			} while(S >= 1 || S == 0);

		X = V1 * sqrt(-2 * log(S) / S);
	} else
		X = V2 * sqrt(-2 * log(S) / S);

	phase = 1 - phase;

	return X * desviacion_estandar;
}

void distribuye_picos(int num_picos, double *picos, double w_max, double lambda)
{
	/*
	Esta función xera unha distribución aleatoria (cunha certa homoxeneidade)
	do número de picos que se lle indique.
	*/

	double separacion = (w_max - 0.05 * w_max) / (num_picos + 1);

	srand(time(NULL));

	*picos = 0.05 * w_max / 2;

	for(int i = 0; i < num_picos; i++)
	{
		*(picos + i + 1) = *(picos + i) + separacion * (1 + 2 * float(rand() % 1000001) / 1000000) / 2;
	}
}

void lorentz(double* Y, double* w, double lambda, double A, double rho, int num_picos, int num_w, double w_max, bool ruido)
{
	/*
	Esta función xera un espectro empregando funcións lorentzianas a partir dos parámetros
	que se lle indican con ruído gausiano.
	*/
	
	double* picos;
	double desviacion_estandar;

	desviacion_estandar = (A / lambda) / rho;

	cout << desviacion_estandar;
	
	picos = new double[num_picos];
	distribuye_picos(num_picos, picos, w_max, lambda);

	for(int i = 0; i < num_w; i++)
	{
		*(Y + i) = 0;
		for(int j = 0; j < num_picos; j++)
		{
			*(Y + i) += A * lambda / (pow(lambda, 2) + pow((*(w + i) - *(picos + j)), 2));
		}
		if(ruido == true)
		{
			*(Y + i) += gaussrand(desviacion_estandar);
		}
	}

	delete[] picos;
}

int main()
{
	double *Y, *w;
	double lambda = 0.05, A = 1, rho = 700, w_max = 100;
	int num_picos = 10, num_w = 65536;
	bool ruido = false;

	Y = new double[num_w];
	w = new double[num_w];

	for(int i = 0; i < num_w; i++)
	{
		*(w + i) = i * w_max / num_w;
	}

	lorentz(Y, w, lambda, A, rho, num_picos, num_w, w_max, ruido);

	ofstream espectrosimulado;
	espectrosimulado.open("espectrosimulado.dat");
	for(int i = 0; i < num_w; i++)
	{
		espectrosimulado << *(w + i) << "	" << setprecision(15) << *(Y + i) << endl;
	}
	espectrosimulado.close();

	delete[] w;
	delete[] Y;

	return 0;
}